**Web Scrapper** with data enrichment from the nytimes

current implementation fetching cnn articles
Please note: 

This version also perforing a data enrichment using 2 public api's:

### https://api.nytimes.com (new york times)
### http://diffbot.com (for article text extraction).

## ER Diagram:

![ER](img/er.png)

## Installation:

1.
Use Python 3.5+ preferably

2.
pip install -r requirements.txt


## Running (Usage)
you should run db_create before running web_scrapper:

1.

usage:db_create.py host port user password

e.g:

python.exe db_create.py localhost 3306 root root

2.

e.g:

usage: web_scrapper_launcher.py year month host port user password

python3 web_scrapper_launcher.py 2020 04 localhost 3306 root root