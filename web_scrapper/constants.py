import re
#'Constants relevant for NYT articles:'
MY_KEY = '4z0w58sv8KYyT90MHjbGASafu5k2PQeK'
EMPTY = 0
EMPLOYER_NYT = 'NYT'
EMPLOYER_NYT_ID = 2
# 'Constants relevant for both:'
PUB_DATE = 'pub_date'
WEB_URL = 'web_url'
BYLINE = 'byline'
NONE = 'None'
PERSON = 'person'
LAST_NAME = 'lastname'
MIDDLE_NAME = 'middlename'
FIRST_NAME = 'firstname'
RESPONSE = 'response'
URL_IS_BROKEN = "url is broken"
HREF = 'href'
COMMA = ','
AUTHORS_REPLACE_RULES = {" and": COMMA, " &": COMMA, " by ": COMMA}
REGEX_BEGIN_HTML_LINK = re.compile("^http://")
EMPLOYER_CNN = 'CNN'
EMPLOYER_CNN_ID = 1
URL_SECTION_LOC = 6
HTML_AUTHOR_NAME_LOC = 0
HTML_URL_LOC = 1
FIRST_SPLIT_INSTANCE = 0
LAST_SPLIT_INSTANCE = -1
NOT_BROKEN = 0
BROKEN = 1
DB = 'cnndb'


#diffbot
DIFFBOT_TOKEN = "a75e1e54c4c0b37b1bad1320f54b7cd3"
DIFFBOT_BASE_ADDRESS = "https://api.diffbot.com/v3/article?token={}&url={}"