#!/bin/bash
# sample runner for generating data
YEAR_START=2017
YEAR_END=2020
for (( i = $YEAR_START; i <= $YEAR_END; i++ ))      ### Outer for loop ###
do

    for (( j = 1 ; j <= 12; j++ )) ### Inner for loop ###
    do
          python3 ./web_scrapper_launcher.py $i $j localhost root root
    done

  echo "" #### print the new line ###
done