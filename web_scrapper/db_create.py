import mysql.connector
import argparse

from constants import *


def create_db(host, port, user, password):
    """
    create_db receives as parameters host, user and password, and then connects to mysql
    and create cnn database
    :param host: string
    :param port: int
    :param user: string
    :param password: string
    :return: None
    """
    try:
        cnx = mysql.connector.connect(host=host, port=port, user=user, password=password)
        cur = cnx.cursor()

        cur.execute("CREATE DATABASE {}".format(DB))

        cnx.close()
    except mysql.connector.errors.InterfaceError as e:
        print("Error in <create_db>. (connection error plz check host/port args) Error details: {}".format(e))


def build_db(host, port, user, password):
    """
    build_db receives as parameters host, user and password, and then connects to mysql
    and build the cnn database
    :param host: string
    :param port: int
    :param user: string
    :param password: string
    :return: None
    """
    try:
        cnx = mysql.connector.connect(host=host, port=port, user=user, password=password, database=DB)
        cur = cnx.cursor()

        cur.execute('''CREATE TABLE employers (
                                id INT NOT NULL,
                                name VARCHAR(255),
                                PRIMARY KEY (id))
        ''')

        cur.execute('''CREATE TABLE urls (
                                id INT NOT NULL AUTO_INCREMENT,
                                address VARCHAR(255),
                                news_by INT,
                                is_broken INT,
                                PRIMARY KEY (id),
                                FOREIGN KEY(news_by) REFERENCES employers(id),
                                UNIQUE (address))
        ''')

        cur.execute('''CREATE TABLE section (
                                name VARCHAR(255),
                                url_id INT,
                                PRIMARY KEY (url_id),
                                FOREIGN KEY(url_id) REFERENCES urls(id))
        ''')

        cur.execute('''CREATE TABLE author (
                                id INT NOT NULL AUTO_INCREMENT,
                                name VARCHAR(255),
                                employer INT,
                                url_id INT,
                                PRIMARY KEY (id),
                                FOREIGN KEY(employer) REFERENCES employers(id),
                                FOREIGN KEY(url_id) REFERENCES urls(id))
        ''')

        cur.execute('''CREATE TABLE links (
                                id INT NOT NULL AUTO_INCREMENT,
                                url_id INT,
                                address VARCHAR(255),
                                PRIMARY KEY (id),
                                FOREIGN KEY(url_id) REFERENCES urls(id))
        ''')

        cur.execute('''CREATE TABLE articles (
                                id INT NOT NULL AUTO_INCREMENT,
                                url_id INT,
                                text BLOB(65535),
                                title VARCHAR(255),
                                date DATE,
                                PRIMARY KEY (id),
                                FOREIGN KEY(url_id) REFERENCES urls(id))
        ''')

        sql = "INSERT INTO employers (id, name) VALUES (%s, %s)"
        cur.execute(sql, (int(EMPLOYER_CNN_ID), EMPLOYER_CNN))
        cur.execute(sql, (int(EMPLOYER_NYT_ID), EMPLOYER_NYT))
        cnx.commit()
        cnx.close()

    except mysql.connector.errors.InterfaceError as e:
        print("Error in <build_db>. (connection error plz check host/port args) Error details: {}".format(e))

def main():
    """
    main accepts host, user and password from the user and then execute create_db and build_db
    in order to create and build the database on the user mysql account.
    :return: None
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("host", type=str, help='Host name of mysql account')
    parser.add_argument("port", type=int, help='port number of mysql account')
    parser.add_argument("user", type=str, help='User name of mysql account')
    parser.add_argument("password", type=str, help='Password of mysql account')

    args = parser.parse_args()

    host = args.host
    port = args.port
    user = args.user
    password = args.password

    create_db(host, port, user, password)
    build_db(host, port, user, password)


if __name__ == '__main__' and __package__ is None:
    from os import sys, path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
    main()