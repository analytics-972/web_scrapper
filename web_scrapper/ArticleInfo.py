class ArticleInfo:
  def __init__(self, authors, section, title, text, update_time):
    self.authors = authors
    self.section = section
    self.title = title
    self.text = text
    self.update_time = update_time
