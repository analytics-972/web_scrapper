import requests
from bs4 import BeautifulSoup
import argparse
import mysql.connector
from constants import *
from ArticleInfo import *


'Functions relevant for CNN articles:'


def parse_url(url):
    """
    parse_url receives url address, that was extracted from CNN article map, as a string, and parse this url
    collecting: title, author, update time and section of an article
    :param url: string
    :return: tuple
    """
    s = requests.get(url)
    cur_soup = BeautifulSoup(s.content, 'html.parser')

    title = cur_soup.find('title').get_text()
    print("Title: " + title)

    try:
        update_time = (cur_soup.find("p", {"class": "update-time"}).get_text() or
                       cur_soup.find("div", {"class": "PageHead_published"}).get_text())
        text = get_article_text(url)
    except:
        update_time = None
        text = ""
    print("Update Time: " + str(update_time))
    section = str(url).split('/')[URL_SECTION_LOC]
    print("Section: " + section)

    try:
        authors_str = (cur_soup.find("meta", {"name": "author"})['content']).split(', CNN')[HTML_AUTHOR_NAME_LOC]
        print('Authors: ' + authors_str)
        authors_list = get_authors_list(authors_str)
        return ArticleInfo(authors_list, section, title, text, update_time)

    except TypeError:
        return ArticleInfo([NONE], section, title, text, update_time)


def parse_links(url):
    """
    parse_links receives url address, that was extracted from CNN article map, as a string, and parse this url
    collecting all the links that are in the article
    :param url: string
    :return: list
    """
    s1 = requests.get(url)
    cur_soup1 = BeautifulSoup(s1.content, 'html.parser')

    links = []
    for link in cur_soup1.findAll('a', attrs={HREF: REGEX_BEGIN_HTML_LINK}):
        links.append(link.get(HREF))
        print(link)

    return links


def cnn_article_collector(year, month):
    """
    cnn_articles_collector receives year and month and return a list of all the articles from CNN articles map
    which were published in the input year and month.
    The elements in the returned list are in HTML format.
    :param year: int
    :param month: int
    :return: list
    """
    web_source = requests.get("https://edition.cnn.com/article/sitemap-{}-{}.html".format(year,month))
    soup = BeautifulSoup(web_source.content, "html.parser")

    article_links = soup.findAll("div", {"class": "sitemap-entry"})

    return article_links[HTML_URL_LOC].findAll('a')


def get_authors_list(authors_str):
    """
    get_authors_list receives a str and split it to all the authors who wrote the article
    :param authors_str: string
    :return: list of authors
    """
    for k, v in AUTHORS_REPLACE_RULES.items():
        authors_str = authors_str.replace(k, v)
    authors_list = authors_str.split(COMMA)
    return authors_list


def get_article_text(url):
    """retrieving the article text from the url parameter  using diffBot API"""
    query_url_url = DIFFBOT_BASE_ADDRESS.format(DIFFBOT_TOKEN, url)
    try:
        r = requests.get(query_url_url)
        extraction = r.json()['objects'][0]
        result = extraction['text']
    except:
        result = ''
    return result


def fill_cnn_db(year, month, host, port, user, password):
    """
    fill_cnn_db receives year, month, host, user and password and fills database of the current user with all cnn
    articles info for each year and month
    :param year: int
    :param month: int
    :param host: string
    :param port: int
    :param user: string
    :param password: string
    :return: None
    """
    articles = cnn_article_collector(year, month)

    try:
        cnx = mysql.connector.connect(host=host, port=port, user=user, password=password, database=DB)
        cur = cnx.cursor()

        for deb, article in enumerate(articles):  # deb is for debugging
            url = article[HREF]  # type: object
            deb = deb + 1
            broken = []
            print(str(deb))

            try:
                article_info = parse_url(url)
                link_info = parse_links(url)

                sql = 'INSERT INTO urls (address, news_by, is_broken) VALUES (%s, %s, %s)'
                cur.execute(sql, (url, EMPLOYER_CNN_ID, NOT_BROKEN))
                last_url_id = cur.lastrowid

                sql1 = 'INSERT INTO section (name, url_id) VALUES (%s, %s)'
                cur.execute(sql1, (article_info.section, last_url_id))

                for author in article_info.authors:
                    print('Author: ' + author)
                    sql2 = 'INSERT INTO author (name, employer, url_id) VALUES (%s, %s, %s)'
                    cur.execute(sql2, (author, EMPLOYER_CNN_ID, last_url_id))

                sql4 = 'INSERT INTO links (url_id, address) VALUES (%s, %s)'
                for link in link_info:
                    cur.execute(sql4, (last_url_id, link))

                date = '{}-{}-{}'.format(year, month, update_time_day(article_info.update_time))
                sql5 = 'INSERT INTO articles (url_id, text, title, date) VALUES (%s, %s, %s, %s)'
                cur.execute(sql5, (last_url_id, article_info.text, article_info.title, date))

                print()

            except mysql.connector.errors.IntegrityError:
                print("{} is already in database".format(url))
            except RuntimeError:
                print(URL_IS_BROKEN)
                broken.append(url)

        cnx.commit()
        cnx.close()

        valid_sites = deb - len(broken)
        print("succeeded to parse: " + str(valid_sites) + " of " + str(deb) + ", " + "%.2f" % (100 * (valid_sites / deb))
              + "% valid data")
    except mysql.connector.errors.InterfaceError as e:
        print("Error in <fill_cnn_db>. (connection error plz check host/port args) Error details: {}".format(e))


'Functions relevant for NYT articles:'


def get_author(jsn):
    """
    get_author receives json data and extracts all the authors.
    :param jsn: json data
    :return: list
    """
    try:
        if len(jsn[('%s' % BYLINE)][PERSON]) == EMPTY:
            return [NONE]
    except:
        return [NONE]
    authors = []
    for person in jsn[BYLINE][PERSON]:
        if person[MIDDLE_NAME]:
            authors.append(person[FIRST_NAME] + " " + person[MIDDLE_NAME] + " " + person[LAST_NAME])
        elif person[LAST_NAME]:
            authors.append(person[FIRST_NAME] + " " + person[LAST_NAME])
        elif person[FIRST_NAME]:
            authors.append(person[FIRST_NAME])
    return authors


def fill_nyt_db(year, month, host, port, user, password):
    """
    fill_nyt_db receives year, month, host, user and password and fills database of the current user with all nyt
    articles info for each year and month
    :param year: int
    :param month: int
    :param host: string
    :param port: int
    :param user: string
    :param password: string
    :return: None
    """
    res = requests.get('https://api.nytimes.com/svc/archive/v1/{}/{}.json?api-key={}'.format(year, month, MY_KEY))
    json_data = res.json()

    try:
        cnx = mysql.connector.connect(host=host, port=port, user=user, password=password, database=DB)
        cur = cnx.cursor()

        for deb, item in enumerate(json_data[RESPONSE]['docs']):
            deb = deb + 1
            broken = []
            print(str(deb))

            try:

                print(item[WEB_URL])
                sql = 'INSERT INTO urls (address, news_by, is_broken) VALUES (%s, %s, %s)'
                cur.execute(sql, (item[WEB_URL], EMPLOYER_NYT_ID, NOT_BROKEN))
                last_url_id = cur.lastrowid

                for author in get_author(item):
                    print(author)
                    sql2 = 'INSERT INTO author (name, employer, url_id) VALUES (%s, %s, %s)'
                    cur.execute(sql2, (author, EMPLOYER_NYT_ID, last_url_id))

                print(item['section_name'])
                sql1 = 'INSERT INTO section (name, url_id) VALUES (%s, %s)'
                cur.execute(sql1, (item['section_name'], last_url_id))

                print(item[PUB_DATE])
                print(update_time_day(item[PUB_DATE]))

                date = '{}-{}-{}'.format(year, month, update_time_day(item[PUB_DATE]))
                sql5 = 'INSERT INTO articles (url_id, text, title, date) VALUES (%s, %s, %s, %s)'
                cur.execute(sql5, (last_url_id, "", item['headline']['main'], date))

                print()

            except mysql.connector.errors.IntegrityError:
                print("{} is already in database".format(item[WEB_URL]))
            except mysql.connector.errors.DatabaseError:
                print(URL_IS_BROKEN)
                broken.append(item[WEB_URL])
            except RuntimeError:
                print(URL_IS_BROKEN)
                broken.append(item[WEB_URL])

        cnx.commit()
        cnx.close()

        valid_sites = deb - len(broken)
        print("succeeded to parse: " + str(valid_sites) + " of " + str(deb) + ", " + "%.2f" % (100 * (valid_sites / deb))
              + "% valid data")

    except mysql.connector.errors.InterfaceError as e:
        print("Error in <fill_nyt_db>. (connection error plz check host/port args) Error details: {}".format(e))


'Functions relevant for both:'


def update_time_day(update_time):
    """
    update_time_day receives an update_time string as it appears in CNN article or NYT article, and returns the day
    of the update_time
    :param update_time: string
    :return: int
    """
    if update_time is None:
        return 1
    if '-' in update_time:
        return int(update_time.split('T')[FIRST_SPLIT_INSTANCE].split('-')[LAST_SPLIT_INSTANCE])

    return int(update_time.split(COMMA)[FIRST_SPLIT_INSTANCE].split(' ')[LAST_SPLIT_INSTANCE])


def main():
    """
    main accepts from the user the following: year of article, month of article, host, user, password
    of mysql account, and connects to 'cnndb' in the user's mysql account.
    Then, main parse all of CNN and NYT articles that correspond to the given year and month, and
    save the data in 'cnndb'.
    :return: None
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("year", type=int, help='Year of wanted articles. Form: YYYY')
    parser.add_argument("month", type=int, help='Month of wanted articles. Form: MM')
    parser.add_argument("host", type=str, help='Host name of mysql account')
    parser.add_argument("port", type=int, help='port number of mysql account')
    parser.add_argument("user", type=str, help='User name of mysql account')
    parser.add_argument("password", type=str, help='Password of mysql account')

    args = parser.parse_args()

    year = args.year
    month = args.month
    host = args.host
    port = args.port
    user = args.user
    password = args.password

    fill_cnn_db(year, month, host, port, user, password)
    fill_nyt_db(year, month, host, port, user, password)


if __name__ == '__main__':
    main()
